<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public static $brand = ['Nokian','BFGoodrich','Pirelli','Toyo','Continental','Hankook','Mitas'];
    public static $technologiya = ['RunFlat','Run Flat','ROF','ZP','SSR','ZPS','HRS','RFT'];
    public static $camernost = ['ТТ','TL','TL/TT'];
    public static $sezon = ['Зимние (шипованные)','Внедорожные','Летние','Зимние (нешипованные)','Всесезонные'];

    
    public static function parse(string $name)
    {
        $brand = self::parseSearch(self::$brand, $name);
        $techno = self::parseSearch(self::$technologiya, $name);
        $camernost = self::parseSearch(self::$camernost, $name);
        $sezon = self::parseSearch(self::$sezon, $name);

        $model = self::parseModel($name, isset($brand[2]) ? $brand[2] : 0);
        $props = self::parseProps($name, isset($model[2]) ? $model[2] : 0);


        $data = [
            'brand'  => $brand[0],
            'model'  => $model[0],
            'propertys' => $props[0],
            'technologii' => $techno[0],
            'camernost' => $camernost[0],
            'sezon' => $sezon[0]
        ];

        $data = self::parseIncorrect($data);

        // TODO  Запись в базу значений
        if($data['correct']) {
            
        }

        return $data;
    }



    public static function parseModel(string $name, $offset)
    {
        // $pattern = '/(.*)\s([0-9]+\/[0-9]+)/i';
        $pattern = '/(.*)\s([0-9]+)\/([0-9R]+)\s/i';
        $offset = (int) $offset;
        
        preg_match($pattern, $name, $matches, PREG_OFFSET_CAPTURE, $offset);
        return [
            trim($matches[1][0]),
            $matches[1][1],
            $matches[1][1] + strlen($matches[1][0]),
        ];
    }


    public static function parseProps(string $name, $offset)
    {
        $pattern = '/(\d+)\/(\d+)([a-zA-Z]+)(\d+)\s([0-9\/]+)([a-zA-Z]+)/i';
        $offset = (int) $offset;

        preg_match($pattern, $name, $matches, PREG_OFFSET_CAPTURE);
        return [
            [
                'width'         => $matches[1][0],
                'height'        => $matches[2][0],
                'construction'  => $matches[3][0],
                'diameter'      => $matches[4][0],
                'load_index'    => $matches[5][0],
                'speed_index'   => $matches[6][0],
            ],
            $matches[1][1],
            $matches[6][1] + strlen($matches[6][0])
        ];
    }



    public static function parseIncorrect(array $data)
    {
        $correct = true;

        if(!$data['brand']) $correct = false;
        if(!$data['model']) $correct = false;
        
        foreach($data['propertys'] as $key=>$val)
            if(!$val) $correct = false;


        $data['correct'] = $correct;
        return $data;
    }



    public static function parseSearch(array $data, string $subject)
    {
        $k = 0;
        $result = null;
        while($k < count($data))
        {
            $str = str_replace(
                ['/','(',')'],
                ['\/','\(','\)'], 
                $data[$k]
            );

            $pattern = '/(^|\s)('.$str.')($|\s)/i';
            preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE);
            if(is_array($matches) && count($matches)>1)
            {
                break;
            }
            $k ++;
        }

        if(is_array($matches) && count($matches)>1)
        {
            $result = [
                trim($matches[0][0]),                   // Значение
                $matches[0][1],                         // Начало вхождения символа
                $matches[0][1] + strlen(trim($matches[0][0])) // Конец вхождения символа
            ];
        }

        return $result;
    }

}
