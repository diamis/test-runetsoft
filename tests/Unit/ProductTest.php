<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\Product;

class ProductTest extends TestCase
{

    public function emptyArray($array1, $array2)
    {
        foreach($array1 as $key=>$val)
        {
            if(!is_array($val)) 
            {
                if(strlen($val)>0) {
                    // echo "\n{$val} = {$array2[$key]}";
                    $this->assertSame($val, $array2[$key]);
                }else{
                    $this->assertEmpty($array2[$key]);
                }                
            }
            else
            {
                $this->emptyArray($val, $array2[$key]);
            }
        }
    }


    /**
     * @dataProvider providerProduct
     */
    public function testProduct($productName, $productResult)
    {
        $data = Product::parse($productName);
        $this->assertNotEmpty($data);
        
        $this->emptyArray($data, $productResult);
    }


    public function providerProduct()
    {
        return [
            // [
            //     'Toyo H08 195/75R16C 107/105S TL Летние',
            //     [
            //         'brand' => 'Toyo',
            //         'model' => 'H08',
            //         'propertys' => [
            //             'width' => '195',
            //             'height' => '75',
            //             'construction' => 'R',
            //             'diameter' => '16',
            //             'load_index' => '1',
            //             'speed_index' => '32',
            //         ],
            //         'technologii' => 'SSR',
            //         'camernost' => 'TL',
            //         'sezon' => 'Летние',
            //         'correct' => true,
            //     ]
            // ],
            [
                'Pirelli Winter SnowControl serie 3 175/70R14 84T TL Зимние (нешипованные)',
                [
                    'brand' => 'Pirelli',
                    'model' => 'Winter SnowControl serie 3',
                    'propertys' => [
                        'width' => '175',
                        'height' => '70',
                        'construction' => 'R',
                        'diameter' => '14',
                        'load_index' => '84',
                        'speed_index' => 'T',
                    ],
                    'technologii' => '',
                    'camernost' => 'TL',
                    'sezon' => 'Зимние (нешипованные)',
                    'correct' => true,
                ]
            ],
            [
                'BFGoodrich Mud-Terrain T/A KM2 235/85R16 120/116Q TL Внедорожные',
                [
                    'brand' => 'BFGoodrich',
                    'model' => 'Mud-Terrain T/A KM2',
                    'propertys' => [
                        'width' => '235',
                        'height' => '85',
                        'construction' => 'R',
                        'diameter' => '16',
                        'load_index' => '120/116',
                        'speed_index' => 'Q',
                    ],
                    'technologii' => '',
                    'camernost' => 'TL',
                    'sezon' => 'Внедорожные',
                    'correct' => true,
                ]
            ],
            [
                'Pirelli Scorpion Ice & Snow 265/45R21 104H TL Зимние (нешипованные)',
                [
                    'brand' => 'Pirelli',
                    'model' => 'Scorpion Ice & Snow',
                    'propertys' => [
                        'width' => '265',
                        'height' => '45',
                        'construction' => 'R',
                        'diameter' => '21',
                        'load_index' => '104',
                        'speed_index' => 'H',
                    ],
                    'technologii' => '',
                    'camernost' => 'TL',
                    'sezon' => 'Зимние (нешипованные)',
                    'correct' => true, 
                ]
            ],
            [
                'Pirelli Winter SottoZero Serie II 245/45R19 102V XL Run Flat * TL Зимние (нешипованные)',
                [
                    'brand' => 'Pirelli',
                    'model' => 'Winter SottoZero Serie II',
                    'propertys' => [
                        'width' => '245',
                        'height' => '45',
                        'construction' => 'R',
                        'diameter' => '19',
                        'load_index' => '102',
                        'speed_index' => 'V',
                    ],
                    'technologii' => 'Run Flat',
                    'camernost' => 'TL',
                    'sezon' => 'Зимние (нешипованные)',
                    'correct' => true, 
                ]
            ],
            [
                'Nokian Hakkapeliitta R2 SUV/Е 245/70R16 111R XL TL Зимние (нешипованные)',
                [
                    'brand' => 'Nokian',
                    'model' => 'Hakkapeliitta R2 SUV/Е',
                    'propertys' => [
                        'width' => '245',
                        'height' => '70',
                        'construction' => 'R',
                        'diameter' => '16',
                        'load_index' => '111',
                        'speed_index' => 'R',
                    ],
                    'technologii' => '',
                    'camernost' => 'TL',
                    'sezon' => 'Зимние (нешипованные)',
                    'correct' => true, 
                ]
            ],
            [
                'Pirelli Winter Carving Edge 225/50R17 98T XL TL Зимние (шипованные)',
                [
                    'brand' => 'Pirelli',
                    'model' => 'Winter Carving Edge',
                    'propertys' => [
                        'width' => '225',
                        'height' => '50',
                        'construction' => 'R',
                        'diameter' => '17',
                        'load_index' => '98',
                        'speed_index' => 'T',
                    ],
                    'technologii' => '',
                    'camernost' => 'TL',
                    'sezon' => 'Зимние (шипованные)',
                    'correct' => true, 
                ]
            ],
            [
                'Continental ContiCrossContact LX Sport 255/55R18 105H FR MO TL Всесезонные',
                [
                    'brand' => 'Continental',
                    'model' => 'ContiCrossContact LX Sport',
                    'propertys' => [
                        'width' => '255',
                        'height' => '55',
                        'construction' => 'R',
                        'diameter' => '18',
                        'load_index' => '105',
                        'speed_index' => 'H',
                    ],
                    'technologii' => '',
                    'camernost' => 'TL',
                    'sezon' => 'Всесезонные',
                    'correct' => true, 
                ]
            ],
            [
                'BFGoodrich g-Force Stud 205/60R16 96Q XL TL Зимние (шипованные)',
                [
                    'brand' => 'BFGoodrich',
                    'model' => 'g-Force Stud',
                    'propertys' => [
                        'width' => '205',
                        'height' => '60',
                        'construction' => 'R',
                        'diameter' => '16',
                        'load_index' => '96',
                        'speed_index' => 'Q',
                    ],
                    'technologii' => '',
                    'camernost' => 'TL',
                    'sezon' => 'Зимние (шипованные)',
                    'correct' => true,
                ]
            ],
            [
                'BFGoodrich Winter Slalom KSI 225/60R17 99S TL Зимние (нешипованные)',
                [
                    'brand' => 'BFGoodrich',
                    'model' => 'Winter Slalom KSI',
                    'propertys' => [
                        'width' => '225',
                        'height' => '60',
                        'construction' => 'R',
                        'diameter' => '17',
                        'load_index' => '99',
                        'speed_index' => 'S',
                    ],
                    'technologii' => '',
                    'camernost' => 'TL',
                    'sezon' => 'Зимние (нешипованные)',
                    'correct' => true, 
                ]
            ],
            [
                'Continental ContiSportContact 5 245/45R18 96W SSR FR TL Летние',
                [
                    'brand' => 'Continental',
                    'model' => 'ContiSportContact 5',
                    'propertys' => [
                        'width' => '245',
                        'height' => '45',
                        'construction' => 'R',
                        'diameter' => '18',
                        'load_index' => '96',
                        'speed_index' => 'W',
                    ],
                    'technologii' => 'SSR',
                    'camernost' => 'TL',
                    'sezon' => 'Летние',
                    'correct' => true, 
                ]
            ],
            [
                'Continental ContiWinterContact TS 830 P 205/60R16 92H SSR * TL Зимние (нешипованные)',
                [
                    'brand' => 'Continental',
                    'model' => 'ContiWinterContact TS 830 P',
                    'propertys' => [
                        'width' => '205',
                        'height' => '60',
                        'construction' => 'R',
                        'diameter' => '16',
                        'load_index' => '92',
                        'speed_index' => 'H',
                    ],
                    'technologii' => 'SSR',
                    'camernost' => 'TL',
                    'sezon' => 'Зимние (нешипованные)',
                    'correct' => true,
                ]
            ],
            [
                'Continental ContiWinterContact TS 830 P 225/45R18 95V XL SSR FR * TL Зимние (нешипованные)',
                [
                    'brand' => 'Continental',
                    'model' => 'ContiWinterContact TS 830 P',
                    'propertys' => [
                        'width' => '225',
                        'height' => '45',
                        'construction' => 'R',
                        'diameter' => '18',
                        'load_index' => '95',
                        'speed_index' => 'V',
                    ],
                    'technologii' => 'SSR',
                    'camernost' => 'TL',
                    'sezon' => 'Зимние (нешипованные)',
                    'correct' => true,
                ]
            ],
            [
                'Hankook Winter I*Cept Evo2 W320 255/35R19 96V XL TL/TT Зимние (нешипованные)',
                [
                    'brand' => 'Hankook',
                    'model' => 'Winter I*Cept Evo2 W320',
                    'propertys' => [
                        'width' => '255',
                        'height' => '35',
                        'construction' => 'R',
                        'diameter' => '19',
                        'load_index' => '96',
                        'speed_index' => 'V',
                    ],
                    'technologii' => '',
                    'camernost' => 'TL/TT',
                    'sezon' => 'Зимние (нешипованные)',
                    'correct' => true,
                ]
            ],
            [
                'Mitas Sport Force+ 120/65R17 56W TL Летние',
                [
                    'brand' => 'Mitas',
                    'model' => 'Sport Force+',
                    'propertys' => [
                        'width' => '120',
                        'height' => '65',
                        'construction' => 'R',
                        'diameter' => '17',
                        'load_index' => '56',
                        'speed_index' => 'W',
                    ],
                    'technologii' => '',
                    'camernost' => 'TL',
                    'sezon' => 'Летние',
                    'correct' => true,
                ]
            ],
        ];
    }
}
